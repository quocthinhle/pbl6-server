import BaseController from '../../../common/base/controller.js';
import Response from '../../../common/utils/http-response.js';
// Cache: import Mapper from '../../../frameworks/database/mapper/index.js';

class BookmarkController extends BaseController {
    async getByUser(req, res) {
        const { user } = req;
        const bookmarks = await this.service.getBookmarks({
            user,
        });

        return Response.success({ res, data: bookmarks });
    }

    async bookmark(req, res) {
        const { user } = req;
        const { question } = req.body;

        const bookmark = await this.service.bookmark({
            user,
            question,
        });

        return Response.success({ res, data: bookmark });
    }
}

export default BookmarkController;
