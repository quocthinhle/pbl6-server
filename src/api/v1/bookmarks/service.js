class BookmarkService {
    constructor({ bookmarkRepository, redisClient, logRepository }) {
        this.bookmarkRepository = bookmarkRepository;
        this.redisClient = redisClient;
        this.logRepository = logRepository;
    }

    async bookmark({
        user,
        question,
    }) {
        this.logRepository.create({
            userId: user._id,
            activity: {
                kind: 'bookmark',
                question,
            },
        });

        return await this.bookmarkRepository.create({
            question,
            user: user._id,
        });
    }

    async getBookmarks({
        user,
    }) {
        return await this.bookmarkRepository.find({
            where: {
                user: user._id,
            },
        });
    }
}

export default BookmarkService;
