import AuthMiddleware from '../../../middlewares/auth.js';
import BookmarkController from './controller.js';
import BookmarkService from './service.js';

export default function route({
    express,
    redisClient,
    logRepository,
    bookmarkRepository,
}) {
    const router = express.Router();
    const service = new BookmarkService({
        redisClient,
        bookmarkRepository,
        logRepository,
    });
    const controller = new BookmarkController(service);

    router.route('/')
        .get(
            [
                AuthMiddleware.interceptToken,
            ],
            controller.getByUser.bind(controller),
        )
        .post(
            [
                AuthMiddleware.interceptToken,
            ],
            controller.bookmark.bind(controller),
        );

    return router;
}
