import userRouting from './users/api.js';
import authRouting from './auth/api.js';
import bookmarkRouting from './bookmarks/api.js';
import questionRouting from './questions/api.js';
import logRouting from './logs/api.js';

export {
    logRouting,
    userRouting,
    authRouting,
    bookmarkRouting,
    questionRouting,
};
