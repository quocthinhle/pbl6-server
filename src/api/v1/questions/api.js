import AuthMiddleware from '../../../middlewares/auth.js';
import QuestionController from './controller.js';
import QuestionService from './service.js';

export default function route({
    express,
    redisClient,
    bookmarkRepository,
    logRepository,
}) {
    const router = express.Router();
    const service = new QuestionService({
        redisClient,
        bookmarkRepository,
        logRepository,
    });
    const controller = new QuestionController(service);

    router.route('/')
        .get(
            [
                AuthMiddleware.interceptToken,
            ],
            controller.getRelativeQuestions.bind(controller),
        );

    router.route('/:questionId')
        .get(
            [
                AuthMiddleware.interceptToken,
            ],
            controller.getQuestionInfo.bind(controller),
        );

    router.put('/:questionId/upvote', [
        AuthMiddleware.interceptToken,
    ], controller.upvote.bind(controller));

    router.put('/:questionId/downvote', [
        AuthMiddleware.interceptToken,
    ], controller.downvote.bind(controller));

    return router;
}
