import axios from 'axios';
import fs from 'fs';
import path from 'path';

class QuestionService {
    constructor({ questionRepository, redisClient, logRepository, }) {
        this.questionRepository = questionRepository;
        this.redisClient = redisClient;
        this.logRepository = logRepository;
    }

    async getQuestionInfo({
        questionId,
    }) {
        return this.questionRepository.findOne({
            where: {
                questionId,
            },
        });
    }

    async upvoteQuestion({
        _userId,
        _questionId,
    }) {
        return this.questionRepository.updateOne({});
    }

    async downVote({
        _userId,
        _questionId,
    }) {
        return this.questionRepository.updateOne({});
    }

    async getRelatives(user, q) {
        this.logRepository.create({
            userId: user._id,
            activity: {
                kind: 'search',
                query: q,
            },
        });

        const getQuestionRequest = await axios({
            url: `http://localhost:5000/api/v1/predictions?q=${q}`,
            method: 'GET',
        });

        const questionData = getQuestionRequest && getQuestionRequest.data;

        if (!questionData) {
            return {
                indices: [],
                scores: [],
            };
        }

        const { indices, scores } = questionData;
        // const stackOverflowInfo = await Promise.all(
        //     indices.map(async id => {
        //         const { data } = await axios({
        //             method: 'GET',
        //             url: `https://api.stackexchange.com/2.3/posts/${id}?site=stackoverflow`,
        //         });

        //         return data;
        //     }),
        // );

        // const { indices, scores, stackOverflowInfo } = JSON.parse(fs.readFileSync(path.join(process.cwd(), 'data.json')));

        return {
            indices,
            scores,
        };
    }
}

export default QuestionService;
