import BaseController from '../../../common/base/controller.js';
import Response from '../../../common/utils/http-response.js';
// Cache: import Mapper from '../../../frameworks/database/mapper/index.js';

class QuestionController extends BaseController {
    async getQuestionInfo(req, res) {

    }

    async upvote(req, res) {

    }

    async downvote(req, res) {

    }

    async getRelativeQuestions(req, res) {
        const query = req.query.q;
        const { user } = req;
        const questions = await this.service.getRelatives(user, query);

        console.log(questions);

        return Response.success({ res, data: questions });
    }
}

export default QuestionController;
