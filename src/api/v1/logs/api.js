import AuthMiddleware from '../../../middlewares/auth.js';
import LogController from './controller.js';
import LogService from './service.js';

export default function route({
    express,
    redisClient,
    bookmarkRepository,
    logRepository,
}) {
    const router = express.Router();
    const service = new LogService({
        redisClient,
        bookmarkRepository,
        logRepository,
    });
    const controller = new LogController(service);

    router.route('/')
        .get(
            [
                AuthMiddleware.interceptToken,
            ],
            controller.getLogs.bind(controller),
        );

    return router;
}
