import BaseController from '../../../common/base/controller.js';
import Response from '../../../common/utils/http-response.js';
// Cache: import Mapper from '../../../frameworks/database/mapper/index.js';

class LogController extends BaseController {
    async getLogs(req, res) {
        const { user } = req;
        const activities = await this.service.getActivities({
            userId: user._id,
            limit: 20,
        });

        return Response.success({ res, data: activities });
    }
}

export default LogController;
