class LogService {
    constructor({ redisClient, logRepository }) {
        this.redisClient = redisClient;
        this.logRepository = logRepository;
    }

    async getActivities({
        userId,
        limit = 20,
    }) {
        const activities = await this.logRepository.find({
            limit,
            where: {
                userId,
            },
        });

        return activities;
    }
}

export default LogService;
