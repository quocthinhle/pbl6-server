import mongoose from 'mongoose';

const { Schema, model, SchemaTypes } = mongoose;

const BookmarkSchema = new Schema({
    question: {
        type: SchemaTypes.Mixed,
    },
    user: {
        type: SchemaTypes.ObjectId,
        ref: 'User',
    },
}, {
    collection: 'bookmarks',
    timestamps: true,
});

const BookmarkDao = model('Bookmark', BookmarkSchema);

export default BookmarkDao;
