import mongoose from 'mongoose';

const LogSchema = new mongoose.Schema({
    userId: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true,
    },
    activity: {
        type: mongoose.SchemaTypes.Mixed,
    },
}, {
    timestamps: true,
    collection: 'logs',
});

const LogDao = mongoose.model('Log', LogSchema);

export default LogDao;
