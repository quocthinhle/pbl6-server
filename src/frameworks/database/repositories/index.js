import BaseMongooseRepository from '../../../common/base/mongoose-repository.js';

import UserDao from '../schemas/user.js';
import TokenDao from '../schemas/token.js';
import BookmarkDao from '../schemas/bookmark.js';
import LogDao from '../schemas/log.js';

let userRepository;
let tokenRepository;
let bookmarkRepository;
let questionRepository;
let logRepository;

const MongooseRepositoriesContainer = {
    get() {
        return {
            userRepository,
            tokenRepository,
            bookmarkRepository,
            logRepository,
            questionRepository,
        };
    },
    init() {
        if (!userRepository) {
            userRepository = new BaseMongooseRepository(UserDao);
            tokenRepository = new BaseMongooseRepository(TokenDao);
            bookmarkRepository = new BaseMongooseRepository(BookmarkDao);
            logRepository = new BaseMongooseRepository(LogDao);
        }

        return this;
    },
};

export default MongooseRepositoriesContainer;
